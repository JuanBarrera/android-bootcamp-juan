package com.balth.barrera.micalculadora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var testResult : TextView ?= null
    var num1: Double = 0.0
    var num2: Double = 0.0
    var operation: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        testResult = findViewById(R.id.textResult)
        val buttonZero: Button = findViewById(R.id.buttonZero)
        buttonZero.setOnClickListener { pressNumber("0") }
        val buttonOne: Button  = findViewById(R.id.buttonOne)
        buttonOne.setOnClickListener { pressNumber("1") }
        val buttonTwo: Button  = findViewById(R.id.buttonTwo)
        buttonTwo.setOnClickListener { pressNumber("2") }
        val buttonThree: Button  = findViewById(R.id.buttonThree)
        buttonThree.setOnClickListener { pressNumber("3") }
        val buttonFour: Button   = findViewById(R.id.buttonFour)
        buttonFour.setOnClickListener { pressNumber("4") }
        val buttonFive: Button   = findViewById(R.id.buttonFive)
        buttonFive.setOnClickListener { pressNumber("5") }
        val buttonSix : Button   = findViewById(R.id.buttonSix)
        buttonSix.setOnClickListener { pressNumber("9") }
        val buttonSeven: Button  = findViewById(R.id.buttonSeven)
        buttonSeven.setOnClickListener { pressNumber("7") }
        val buttonEight: Button  = findViewById(R.id.buttonEight)
        buttonEight.setOnClickListener { pressNumber("8") }
        val buttonNine: Button   = findViewById(R.id.buttonNine)
        buttonNine.setOnClickListener { pressNumber("9") }
        val buttonDot: Button   = findViewById(R.id.buttonDot)
        buttonDot.setOnClickListener { pressNumber(".") }

        val buttonPlus: Button = findViewById(R.id.buttonPlus)
        buttonPlus.setOnClickListener { pressFunction(SUM) }
        val buttonSubs: Button = findViewById(R.id.buttonLess)
        buttonSubs.setOnClickListener { pressFunction(SUBS) }
        val buttonMult: Button = findViewById(R.id.buttonMul)
        buttonMult.setOnClickListener { pressFunction(MULT) }
        val buttonDiv: Button = findViewById(R.id.buttonDivider)
        buttonDiv.setOnClickListener { pressFunction(DIV) }

        val buttonClear: Button = findViewById(R.id.buttonClear)
        buttonClear.setOnClickListener {
            num1 = 0.0
            num2 = 0.0
            testResult?.text = null
            operation = NO_OPERATION
        }

        val buttonEqual: Button = findViewById(R.id.buttonEqual)
        buttonEqual.setOnClickListener {
            var result = when(operation){
                SUM -> num1 + num2
                SUBS -> num1 - num2
                MULT -> num1 * num2
                DIV -> num1 / num2
                else -> null
            }
            testResult?.text = result.toString()
        }
    }

    private fun pressNumber(digit: String){
        testResult?.text = "${testResult?.text}$digit"
        if(operation == NO_OPERATION){
            num1 = testResult?.text.toString().toDouble()
        }else{
            num2 = testResult?.text.toString().toDouble()
        }
    }

    private fun pressFunction(operation: Int){
        this.operation = operation
        testResult?.text=null
    }

    companion object{
        const val SUM  = 1
        const val SUBS = 2
        const val MULT = 3
        const val DIV  = 4
        const val NO_OPERATION = 0
    }
}