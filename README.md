# Android Bootcamp Juan


## POO Programación orientada a objetos:

### Paradigma para modelar objetos reales dentro de un lenguaje de programación basado en objetos de los cuales se puede dividir en dos:

- ### Objetos concretos: objetos palpables que puedo modelar( pantalla, auto)

- ### Objetos abstractos: Conceptos de algo que existe (cuenta bancaria, sentimientos ,...)

### Los objetos se caracterizan por tener atributos y métodos.

- ### Un auto tiene atributos de color, numero de puertas, .... y metodos de encendido , apagado , acelerar, ....

### Para construir un objeto se necesita de una plantilla llamada clase de la cual puede crear múltiples objetos.

- ### Características de una clase: nombre, atributos y métodos.

### Ejemplo clase en Java

    public class Book  {
        private String title;
        private Long isbn;
        public Book(String title, Long isbn) {
            this.title = title;
            this.isbn = isbn;
        }
        public String getTitle() {
            return title;
        }
        public void setTitle(String title) {
            this.title = title;
        }
        public Long getIsbn() {
            return isbn;
        }
        public void setIsbn(Long isbn) {
            this.isbn = isbn;
        }
    }

### Ejemplo Clase en Kotlin

    class Book {
        var title: String
        var isbn: Long

        constructor(title: String, isbn: Long) {
            this.title = title
            this.isbn = isbn
        }
    }

### Otro ejemplo de clase en Kotlin

    class Book constructor(title: String, isbn: Long) {
        var title: String
        var isbn: Long

        init {
        this.title = title
        this.isbn = isbn
        }
    }

    -------------

    class Book constructor(var title: String, var isbn: Long)


## SOLID

---

Ventajas de implementar los principios SOLID

1.  Código legible y mantenido en el tiempo.
2.  Integrar funcionalidades de manera simple.
3.  Calidad del código.

### "S" Principio de responsabilidad Única

### Cada clase solo tiene un motivo para cambiar, cada clase se recomienda que solo tenga una función.

- ### Codigo ejemplo de una clase que tiene mas de una funcionalidad:

        class Perro (raza : String){
            fun savePerroDB(perro : Perro){ ... }
            fun deletePerroDB(id : Int){ ... }
        }

- ### Se cambiaría por dos clases: una para almacenar la raza del perro y otra para ejecutar las operaciones de base de datos.

        class Perro (raza : String)

        class PerroDb{
            fun savePerroDB(perro : Perro){ ... }
            fun deletePerroDB(id : Int){ ... }
        }

### "O" Principio abierto-cerrado

- ### Clase debe esta abierta para su extensión y cerrada para modificarse

  ### En el siguiente ejemplo en caso de que se quieren seguir agregando figuras se tendría que modificar la clase AreaFactory para calcular su respectiva área.

        class Rectangle {
            private var length;
            private var height;
        }

        class Circle {
            private var radius;
        }

        class AreaFactory {
            public fun calculateArea(shapes: ArrayList<Object>): Double {
                var area = 0;
                for (shape in shapes) {
                    if (shape is Rectangle) {
                        var rect = shape as Rectangle;
                        area += (rect.getLength() * rect.getHeight());
                    } else if (shape is Circle) {
                        var circle = shape as Circle;
                        area += (circle.getRadius() * cirlce.getRadius() * Math.PI);
                    } else {
                        throw new RuntimeException("Shape not supported");
                    }
                }
                return area;
            }
        }

  ### Para aplicar el principio abierto-cerrado modificamos de la siguiente manera, implementando una interfaz "Shape" la cual sobreescribirá cada figura que se agregue.

        public interface Shape {
            fun getArea();
        }

        public class Rectangle: Shape {
            private var length;
            private var height;
            // getters/setters ...
            @Override
            public getArea(): Double {
                return (length * height);
            }
        }

        public class Circle : Shape{
            private var radius;
            // getters/setters ...
            @Override
            public getArea() : Double {
                return (radius * radius * Math.PI);
            }
        }

        public class AreaFactory {
            fun calculateArea(shapes: ArrayList<String>): Double {
            var area: Double = 0.toDouble();
            for (shape in shapes) {
                    area += shape.getArea();
                }
                return area;
            }
        }

### "L" Principio de sustitución de Liskov

- ### Implementar subclases que dependan de clases sin realizar cambios en el programa, es decir la clase hijo puede ser reemplazable para la clase padre.

  ### Ejemplo donde se tiene una clase vehículo y varias subclases que heredan los métodos de Vehicle, pero hay clases que no son válidas para esta herencia, por lo que se tendría que modificar la clase padre, incumpliendo en criterio de Liskov.

        abstract class Vehicle{
            protected var isEngineWorking = false
            abstract fun startEngine()
            abstract fun stopEngine()
            abstract fun moveForward()
            abstract fun moveBack()
        }
        class Car: Vehicle(){
            override fun startEngine() {
                println("Engine started")
                isEngineWorking = true
            }

            override fun stopEngine() {
                println("Engine stopped")
                isEngineWorking = false
            }

            override fun moveForward() {
                println("Moving forward")
            }

            override fun moveBack() {
                println("Moving back")
            }
        }
        class Bicycle: Vehicle() {
            override fun startEngine() {
                // TODO("Not yet implemented")
            }

            override fun stopEngine() {
                // TODO("Not yet implemented")
            }

            override fun moveForward() {
                println("Moving forward")
            }

            override fun moveBack() {
                println("Moving back")
            }
        }

  ### Modificación para cumplir criterio usuando una interfaz que agrupe la estructura básica de los vehículos.

        interface Vehicle{
            fun moveForward()
            fun moveBack()
        }

        abstract class VehicleWithEngine: Vehicle{
            private var isEngineWorking = false
            open fun startEngine(){ isEngineWorking = true }
            open fun stopEngine(){ isEngineWorking = false }
        }

        class Car: VehicleWithEngine(){
            override fun startEngine() {
                super.startEngine()
                println("Engine started")
            }

            override fun stopEngine() {
                super.stopEngine()
                println("Engine stopped")
            }

            override fun moveForward() {
                println("Moving forward")
            }

            override fun moveBack() {
                println("Moving back")
            }
        }
        class Bicycle: Vehicle{
            override fun moveForward() {
                println("Moving forward")
            }

            override fun moveBack() {
                println("Moving back")
            }
        }

### "I" Principio de Segregación de interfaz

- ### Evitar crear interfaces grandes: crear interfaces mínimas que agrupen las características comunes de las subclases.

* ### código ejemplo:

        interface Animal{
            fun eat()
            fun sleep()
        }

        interface FlyingAnimal{
            fun fly()
        }

        class Cat: Animal{
            override fun eat() {
                println("Cat is eating fish")
            }

            override fun sleep() {
                println("Cat is sleeping on its owner's bed")
            }
        }

        class Bird: Animal, FlyingAnimal{
            override fun eat() {
                println("Bird is eating forage")
            }

            override fun sleep() {
                println("Bird is sleeping in the nest")
            }

            override fun fly() {
                println("Bird is flying so high")
            }
        }

### "D" principio de inversión de dependencias

- ### Módulos de alto nivel no dependen de los de bajo nivel.

* ### Código Ejemplo:

        interface MobileDeveloper{
            fun developMobileApp()
        }

        class AndroidDeveloper(var mobileService: MobileServices): MobileDeveloper{
            override fun developMobileApp(){
                println("Developing Android Application by using Kotlin. " +
                        "Application will work with ${mobileService.serviceName}")
            }
            enum class MobileServices(var serviceName: String){
                HMS("Huawei Mobile Services"),
                GMS("Google Mobile Services"),
                BOTH("Huawei Mobile Services and Google Mobile Services")
            }
        }

        class IosDeveloper: MobileDeveloper{
            override fun developMobileApp(){
                println("Developing iOS Application by using Swift")
            }
        }

        fun main(){
            val developers = arrayListOf(AndroidDeveloper(AndroidDeveloper.MobileServices.HMS), IosDeveloper())
            developers.forEach { developer ->
                developer.developMobileApp()
            }
        }



## Programación funcional

---
### Paradigma de programación (Forma de resolver problemáticas) y trabajó principalmente con funciones (control de flujo se delega a funciones), se centra en cómo se hace esta resolución de problemas y no en el cómo se está haciendo.
* ### Ejemplo usando javascript para convertir los elementos de un objeto en un solo String
        const languages = Object.values(countri.languages).reduce((acc, language, index) => index === 0 ? (language) : (acc + ", " + language), "")
 
### Conceptos basicos programacion funcional:
1. ### Funciones puras : Funciones que sin importar su entrada la salida es la misma.
        some = fun(1) ; res => 2
        someTwo = fun(2); res => 2
2. ### Composición de funciones: Combinar dos o mas funciones y ejecutar estas en secuencia.
        fnOne = g(x)
        fnTwo = f(x)
        fnSome = f(g(x))
3. ### Estado compartido: Cualquier espacio en memoria que exista en un ámbito compartido  (Alcance global o ámbitos cerrados)
 
4. ### Inmutabilidad: objeto al ser creado su valor no se modifica.
 
 
### Librerias de interés kotlin funcional
Libreria Arrow[Link](https://arrow-kt.io/ "Arrow ").

## Comandos git

---

### Clonar repositorio

    git clone "url del repositorio"

### Crear una rama nueva

    git checkout -b "Nombre de la rama"

### Navegar entre ramas

    git checkout "Nombre de la rama"

### Devolver una rama a su estado anterior:

Borra todos los commit especificados por el numero del 'HEAD' en la rama
en la que este actualmente.

| Por método hard         | Por método soft         | Por método mixed         |
| ----------------------- | ----------------------- | ------------------------ |
| sintaxis:               | sintaxis:               | sintaxis:                |
| git reset --hard HEAD~1 | git reset --soft HEAD~1 | git reset --mixed HEAD~1 |

Estos metodos solo cambian las ramas en local por lo cual para modificar la rama remota se debe de implementar un push. Remitace a "Enviar los cambios a repositorio remoto".

### Consultar los datos modificados del repo y su estado:

    git status

### Agregar todos los cambios al repositorio local:

    git add .

### Agrupar los cambios en un commit:

    git commit -m "mensaje del commit"

### Enviar los cambios a repositorio remoto:

    git push origin "nombre de la rama"

### Traer todos los cambios del repositorio remoto al local de la rama en la esté:

    git pull

### Unir dos ramas:

    git merge "nombre de la rama"

### Renombrar rama en local:

### Estando en una rama distinta a la que se desee cambiar el nombre

    git branch -m "nombre original" "nombre por el que cambiará"
