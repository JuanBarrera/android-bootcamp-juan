package com.balth.barrera.companyapp

public class Accountant : Employee(){
    override val hourly = 50
    override val hours = 200
    override val hasBonus = false
}