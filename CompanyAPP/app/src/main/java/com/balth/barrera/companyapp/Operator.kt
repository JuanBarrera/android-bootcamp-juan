package com.balth.barrera.companyapp

class Operator : Employee(){
    override val hourly = 10
    override val hours = 230
    override val hasBonus = true
}