package com.balth.barrera.companyapp

class Manager : Employee() {
    override val hourly = 200
    override val hours = 200
    override val hasBonus = false
}