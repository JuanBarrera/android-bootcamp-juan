package com.balth.barrera.companyapp

abstract class Employee {
    abstract val hourly : Int
    abstract val hours : Int
    abstract val hasBonus : Boolean
}