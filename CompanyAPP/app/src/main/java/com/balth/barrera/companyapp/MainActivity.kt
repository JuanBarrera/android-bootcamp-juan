package com.balth.barrera.companyapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private val manager = Manager()
    private val operator = Operator()
    private val accountant = Accountant()
    var resultMinWage : TextView ?=null
    var resultBono : TextView ?=null
    var finalResult : TextView ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Log.d("myTag", manager.hourly.toString());
        resultMinWage= findViewById(R.id.resultMinWage)
        resultBono = findViewById(R.id.resultBono)
        finalResult= findViewById(R.id.finalResult)
        val buttonOperator: Button = findViewById(R.id.buttonOperator)
        buttonOperator.setOnClickListener {pressEmployee(operator.hourly,operator.hours,operator.hasBonus)}
        val buttonManager: Button = findViewById(R.id.buttonManager)
        buttonManager.setOnClickListener {pressEmployee(manager.hourly,manager.hours,manager.hasBonus)}
        val buttonAccountant: Button = findViewById(R.id.buttonAccountant)
        buttonAccountant.setOnClickListener {pressEmployee(accountant.hourly,accountant.hours,accountant.hasBonus)}
    }
    private fun pressEmployee(hourly:Int,hours:Int,hasBonus:Boolean){
        val result = hourly*hours
        var resultBonus = 0
        resultMinWage?.text = result.toString()
        if(hasBonus){
            resultBonus = 10*hourly
            resultBono?.text = resultBonus.toString()
        }
        val final =resultBonus + result
        finalResult?.text = final.toString()
    }
}